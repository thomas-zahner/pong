#pragma once

class Random
{
public:
  Random();
  ~Random();

  bool getBool();
  int getInt(int max, int min = 0);
  float getFloat(float max, float min = 0.0f);
};
