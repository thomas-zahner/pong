#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include "Level.h"

class Engine
{
public:
  Engine(const int width, const int height, const char* windowTitle);
  ~Engine();
  static Level* getLevel();
  static bool getDebugMode();
private:
  sf::RenderWindow* window;
  static Level* level;

  void run();
  void createWindow(int width, int height);
  void initializeLevel(int width, int height);

  void tick();
  void render(float partialTicks);

  const char* windowTitle;
};
