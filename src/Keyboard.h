#pragma once
#include <SFML/Graphics.hpp>

class Keyboard
{
public:
  static bool keyUp();
  static bool keyDown();
};
