#pragma once
#include "SFML/Graphics.hpp"
#include "Logger.h"

namespace VectorMath
{
  template <class T>
  T getLength(const sf::Vector2<T>& vector);

  template <class T>
  void logVector(const sf::Vector2<T>& vector, Logger::LogLevel level);

  template <class T>
  float getAngle(const sf::Vector2<T>& vector);
}

#include "VectorMath.tpp"
