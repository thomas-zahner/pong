#include "Engine.h"
#include "Paddle.h"
#include "Logger.h"
#include "VectorMath.h"
#include "Ball.h"

Paddle::Paddle(bool isPlayer) : player(isPlayer)
{
  speed = sf::Vector2f(0.0f, 7.5f);
  setVelocityDeceleration(0.25f);

  if (!texture.loadFromFile("res/paddle.png")) LOG(Logger::ERROR) << "Unable to load the paddle texture";
  loadSpriteFromTexture();

  setHitbox((sf::Vector2f) texture.getSize());

  const float offset = 30.0f;
  const float height = Engine::getLevel()->getSize().y / 2;

  if(player) initPosition(sf::Vector2f(offset, height));
  else initPosition(sf::Vector2f(Engine::getLevel()->getSize().x - offset, height));
}

Paddle::~Paddle()
{
}

void Paddle::tickEntity()
{
  if(player)
  {
    if(!(Keyboard::keyDown() && Keyboard::keyUp()))
    {
      if(Keyboard::keyDown()) velocity = speed;
      if(Keyboard::keyUp()) velocity = -speed;
    }
  }
  else
  {
    for(Ball* ball : Engine::getLevel()->getEntities<Ball>())
      this->position.y = ball->getPosition()->y;
  }

  sf::FloatRect hitbox = getHitbox();

  if(position.y - hitbox.height / 2 < 0) position.y = hitbox.height / 2;

  float levelHeight = Engine::getLevel()->getSize().y;
  if(position.y + hitbox.height / 2 > levelHeight) position.y = levelHeight - hitbox.height / 2;
}

void Paddle::render(sf::RenderWindow &rw, float partialTicks)
{
  sprite.setPosition(*getPreviousPosition() + (position - *getPreviousPosition()) * partialTicks);
  rw.draw(sprite);
}

bool Paddle::isPlayer()
{
  return player;
}
