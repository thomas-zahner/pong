#pragma once
#include <SFML/Graphics.hpp>
#include "Entity.h"
#include "Keyboard.h"

class Paddle : public Entity
{
  public:
    Paddle(bool isPlayer);
    ~Paddle();
    void tickEntity() override;
    void render(sf::RenderWindow &rw, float partialTicks) override;
    bool isPlayer();
  private:
    bool player;
    sf::Vector2f speed;
};
