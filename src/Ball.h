#pragma once
#include <SFML/Graphics.hpp>
#include "Entity.h"
#include "Paddle.h"

class Ball : public Entity
{
  public:
    Ball(sf::Vector2f spawnPoint);
    ~Ball();
    void render(sf::RenderWindow &rw, float partialTicks) override;
    void tickEntity() override;
  protected:
  private:
    sf::Vector2f spawnPoint;
    sf::Vector2f initialVelocity;
    bool previouslyCollided = false;

    void wallBounce();
    void score();
    void respawn();
    void paddleCollision();
    void normalPaddleBounce(Paddle* paddle);
    void sideBounce(Paddle* paddle);
    bool isCollidedSidewaysWith(Paddle* paddle);
};
