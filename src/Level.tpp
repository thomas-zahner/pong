template<class T>
std::vector<T*> Level::getEntities()
{
  std::vector<T*> entitiesOfType;

  for(auto entity : entities)
  {
    T* entityT = dynamic_cast<T*>(entity);
    if(entityT) entitiesOfType.push_back(entityT);
  }

  return entitiesOfType;
}
