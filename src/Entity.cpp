#include "Entity.h"
#include "VectorMath.h"

Entity::Entity()
{
}

Entity::~Entity()
{
}

void Entity::tick()
{
  move();
  tickEntity();
}

void Entity::move()
{
  previousPosition = position;
  position += velocity;
  velocity *= 1.0f - movementDeceleration;
}

void Entity::renderVelocityVector(sf::RenderWindow &rw)
{
  const float vectorWidth = 2.0f;
  const float vectorScale = 5.0f;
  const float angle = VectorMath::getAngle(velocity);
  const float speedSum = abs(this->velocity.x) + abs(this->velocity.y);

  sf::RectangleShape vector(sf::Vector2f(speedSum * vectorScale, vectorWidth));
  vector.setFillColor(sf::Color(0, 255, 0));
  vector.setPosition(this->position + sf::Vector2f(vectorWidth / 2, 0));
  vector.setRotation(angle);

  rw.draw(vector);
}

void Entity::renderHitbox(sf::RenderWindow& rw)
{
  sf::FloatRect hitbox = getHitbox();

  sf::RectangleShape rectangle;
  rectangle.setPosition(hitbox.left, hitbox.top);
  rectangle.setSize(sf::Vector2f(hitbox.width, hitbox.height));

  rectangle.setFillColor(sf::Color(0, 0, 0, 0));
  rectangle.setOutlineThickness(2);
  rectangle.setOutlineColor(sf::Color(255, 0, 0));

  rw.draw(rectangle);
}

bool Entity::isCollidedWith(Entity* other)
{
  return getHitbox().intersects(other->getHitbox());
}

sf::Vector2f* Entity::getPosition()
{
  return &position;
}

sf::Vector2f* Entity::getVelocity()
{
  return &velocity;
}

sf::FloatRect Entity::getHitbox()
{
  return sf::FloatRect(position - hitboxSize / 2.0f, hitboxSize);
}

sf::Vector2f* Entity::getPreviousPosition()
{
  return &previousPosition;
}

void Entity::loadSpriteFromTexture()
{
  sprite = sf::Sprite(texture);
  sprite.setOrigin((sf::Vector2f) texture.getSize() / 2.0f);
}

void Entity::setVelocityDeceleration(float deceleration)
{
  if(deceleration < 0.0f) deceleration = 0.0f;
  else if(deceleration > 1.0f) deceleration = 1.0f;

  movementDeceleration = deceleration;
}

void Entity::initPosition(const sf::Vector2f& position)
{
  this->position = position;
  previousPosition = position;
}

void Entity::setHitbox(const sf::Vector2f& size)
{
  this->hitboxSize = size;
}
