#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
#include "Entity.h"

class Level
{
 public:
  Level(sf::Vector2f size);
  ~Level();
  sf::Vector2f getSize();

  void render(sf::RenderWindow &rw, float partialTicks);
  void tick();

  void add(Entity* entity);

  template<class T>
  std::vector<T*> getEntities();
 protected:
 private:
  sf::Vector2f size;
  std::vector<Entity*> entities;
};

#include "Level.tpp"
