#pragma once
#include <SFML/Graphics.hpp>
#include "Random.h"

class Entity
{
public:
  Entity();
  virtual ~Entity();
  void tick();
  virtual void render(sf::RenderWindow &rw, float partialTicks) = 0;

  void move();
  void renderVelocityVector(sf::RenderWindow &rw);
  void renderHitbox(sf::RenderWindow &rw);
  bool isCollidedWith(Entity* other);

  sf::Vector2f* getPosition();
  sf::Vector2f* getVelocity();
  sf::FloatRect getHitbox();
protected:
  virtual void tickEntity() = 0;

  sf::Vector2f position;
  sf::Vector2f velocity;

  sf::Texture texture;
  sf::Sprite sprite;

  Random random;

  sf::Vector2f* getPreviousPosition();

  void loadSpriteFromTexture();

  void setVelocityDeceleration(float deceleration);
  void initPosition(const sf::Vector2f& position);
  void setHitbox(const sf::Vector2f& size);
private:
  sf::Vector2f hitboxSize;

  float movementDeceleration = 1.0f;
  sf::Vector2f previousPosition;
};
