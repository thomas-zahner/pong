#include "Engine.h"
#include "Logger.h"

#include "Ball.h"
#include "Paddle.h"

#define DEBUG_MODE true

Level* Engine::level;

Engine::Engine(const int width, const int height, const char* windowTitle)
{
  this->windowTitle = windowTitle;

  createWindow(width, height);
  initializeLevel(width, height);

  LOG(Logger::INFO) << "Starting game";
  run();

  LOG(Logger::INFO) << "Stopping game";
  window->close();
}

Engine::~Engine()
{
  delete window;
  delete level;
}

Level* Engine::getLevel()
{
  return level;
}

bool Engine::getDebugMode()
{
  return DEBUG_MODE;
}

void Engine::createWindow(int width, int height)
{
  LOG(Logger::INFO) << "Creating window";
  window = new sf::RenderWindow(sf::VideoMode(width, height), windowTitle, sf::Style::Titlebar | sf::Style::Close);

  window->setFramerateLimit(200);
}

void Engine::initializeLevel(int width, int height)
{
  Engine::level = new Level(sf::Vector2f(width, height));

  level->add(new Ball(Engine::level->getSize() / 2.0f));
  level->add(new Paddle(true));
  level->add(new Paddle(false));
}

void Engine::run()
{
  sf::Clock clock;
  sf::Time accumulator = sf::Time::Zero;
  sf::Time statisticAccumulator = sf::Time::Zero;

  const sf::Time timePerTick = sf::seconds(1.0f / 60);
  unsigned int frameCounter = 0, tickCounter = 0;

  for(;;)
  {
    // Process events
    sf::Event event;
    while(window->pollEvent(event))
    {
      if(event.type == sf::Event::Closed) return;
    }

    while(accumulator > timePerTick)
    {
      accumulator -= timePerTick;
      this->tick();
      tickCounter++;
    }

    this->render(accumulator.asSeconds() / timePerTick.asSeconds());
    frameCounter++;

    statisticAccumulator += clock.getElapsedTime();
    accumulator += clock.restart();

    if(statisticAccumulator > sf::seconds(1.0f))
    {
      statisticAccumulator -= sf::seconds(1.0f);

      window->setTitle(windowTitle + (" (" + std::to_string(frameCounter) + " FPS, " + std::to_string(tickCounter) + " TPS)"));
      frameCounter = 0;
      tickCounter = 0;
    }
  }
}

void Engine::tick()
{
  level->tick();
}

void Engine::render(float partialTicks)
{
  window->clear(sf::Color());
  level->render(*window, partialTicks);
  window->display();
}
