#include "Level.h"
#include "Entity.h"
#include "Engine.h"

Level::Level(sf::Vector2f size)
{
  this->size = size;
}

Level::~Level()
{
  for(auto entity : entities)
    delete entity;
}

void Level::tick()
{
  for(auto entity : entities)
    entity->tick();
}

void Level::render(sf::RenderWindow &rw, float partialTicks)
{
  for(auto entity : entities)
  {
    entity->render(rw, partialTicks);
    if(Engine::getDebugMode())
    {
      entity->renderVelocityVector(rw);
      entity->renderHitbox(rw);
    }
  }
}

void Level::add(Entity* entity)
{
  entities.push_back(entity);
}

sf::Vector2f Level::getSize()
{
  return size;
}
