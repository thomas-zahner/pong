#include <math.h>

namespace VectorMath
{
  template<class T>
  T getLength(const sf::Vector2<T>& vector)
  {
    return (T) sqrt(pow(vector.x, 2) + pow(vector.y, 2));
  }

  template<class T>
  void logVector(const sf::Vector2<T>& vector, Logger::LogLevel level)
  {
    LOG(level) << "(" << vector.x << "|" << vector.y << ")";
  }

  template<class T>
  float getAngle(const sf::Vector2<T>& vector)
  {
    return (float) atan2(vector.y, vector.x) * 180 / M_PI;
  }
}
